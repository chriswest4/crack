#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

int main(int argc, char *argv[]) {
    if (argc < 3) {
        printf("Hashpass must have 2 arguments.\nFormat required:\n\t./hashpass inputfile outputfile\n");
        exit(0);
    }
    
    // Open files
    FILE *inputfile = fopen(argv[1], "r");
    if (!inputfile) {
        printf("Could not open %s", argv[1]);
    }
    FILE *outputfile = fopen(argv[2], "a");
    if (!outputfile) {
        printf("Could not open %s", argv[2]);
    }
    char pass[16];
    while (fscanf(inputfile, " %s", pass) != EOF) {
        char *hash = md5(pass, (strlen(pass)));
        fprintf(outputfile, "%s\n", hash);
        free(hash);
    }
    // Close files
    fclose(inputfile);
    fclose(outputfile);
}